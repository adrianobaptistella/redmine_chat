# README #

This is a simple redmine plugin for chat.

This plugin is open source and released under the terms of the GNU General Public License v2 (GPL).

## What is this repository for? ##

### Quick summary ###

This is a simple redmine plugin for chat. 

As the initial version, it is best to have some experience in the development of skills. Please note the following constraints:

Only deployed in a production mode, http instead of https

Tested only on sqlite3, later test for mysql.

Currently only supporting 3.2.0

### Version ###
0.0.6

mysql2 supported

adjusted the project module's name

the issue's no can be auto-linked

added the permissions for secure of the page accessing

fix the avater's shown in chat box 

bugs known:

  it sometimes doesn't work that minimizing or maximizing the chat box on my development enviroment. it will be fixed in the future
  
  when leave from the client(disconnected from faye channel) the online's recored maybe not be destroyed    

0.0.5

the default chat of auto-pop is first minimized unless it's mannual launched

0.0.4

optimised for the chat quick auto-pop feature when every page loading

it works in chrome with the max or min state but only open max in IE  


0.0.3

extended the chat box to other main pages

0.0.2

began to custome the private_pub gem for redmine_chat plugin

finished the basic chat function
 

0.0.1

first init and propotype demo

## How do I get set up? ##

### Summary of set up ###

First briefly talk to you, it's not recommended to install it unless the future version of 0.0.5

1, the installation process:

Git from bitbucket in the plugins direction: git clone https://39648421@bitbucket.org/39648421/redmine_chat.git

Or download it from bitbucket and expand zip file into the plugins directory and name it by redmine_chat

In redmine home directory, run: bundle install --without development test.

Please run bundle update private_pub if you have bundled from tigergm's github
 
In redmine home directory, run: RAILS_ENV=production rails g private_pub:install. It will generate a few template files

Edit private_pub.ru file in redmine home directory, such as changes to the mysql sqlite3

Edit private_pub.yml file in config directory, such as the server production according to the the server's ip or domain name

In redmine main directory, start faye server, distribute and monitor chat messages, it may be necessary preceded bundle exec. You can add "-D" in the end as backgound daemon. Run: 

rackup private_pub.ru -s thin -E production

Other example if you want to listen all:

rackup -o 0.0.0.0 private_pub.ru -s thin -E production


Migrate database files in redmine home directory, run:

bundle exec rake redmine:plugins:migrate NAME=redmine_chat RAILS_ENV=production

Restart redmine and use the plugin.

### Version Planning ###

Currently limited in the chat section, it will be extended to all pages

Currently automatic pop-up chat mode is maximized, it will be changed to minimize mode to prevent from interference with work

Online status updated in real time in the future, the current state of the background is real-time, but front page refreshes to update the status only

The chat will flash when new message arrived

Support more databases

If redmine is upgraded to rails 5, the plugin will enable action cable mechanism

Support issue, wiki links in conversations, etc.