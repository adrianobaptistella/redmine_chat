class CreateChatUsers < ActiveRecord::Migration
  def change
    create_table :chat_users do |t|
      t.string :client_id
      
      t.references :user, index: true

      t.references :project, index: true

      t.references :conversation, index: true

      t.string :status

      t.timestamps null: false

    end
  end
end
