# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html
resources :conversations,:only => [:show, :create]
resources :projects do
  resources :conversations
  resources :chat_messages
  resources :chat_users, :only => :index
    
  match 'chat_settings/:action' => 'chat_settings', :via => [:get, :post]
end

get 'projects/conversations/:id', :to => 'conversations#show'
get 'projects/:id/issues/conversations/:id', :to => 'conversations#show'
get 'projects/:id/settings/conversations/:id', :to => 'conversations#show'

resources :conversations do
  resources :chat_messages
end