Redmine::Plugin.register :redmine_chat do
  name 'Redmine Chat plugin'
  author 'Tigergm Wu'
  description 'This is a chat plugin for Redmine'
  version '0.0.6'
  url 'https://bitbucket.org/39648421/redmine_chat'
  author_url 'https://bitbucket.org/39648421'
  
  requires_redmine :version_or_higher => '3.2'
  
  project_module :redmine_chat do
    permission :launch_chat, :chat_users => [:index]
    
    permission :manage_chat, {
      :projects => :settings,
      :chat_settings => :save,
    }
    
    permission :show_all_chat_messages, :chat_messages => :index
    permission :delete_chat_messages, :chat_messages => :destroy
    permission :delete_own_chat_messages, :chat_messages => :destroy   
  end
  
  menu :project_menu, :chat, {:controller => 'chat_users', :action => 'index'}, :caption => :label_chat, :param => :project_id
end

require 'redmine_chat'