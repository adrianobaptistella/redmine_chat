class ChatUsersController < ApplicationController
  unloadable

  before_action :find_optional_project, :only => :index
  before_action :clear_old_records, :only => :index

  def index
    @users = @project.users
    
    @conversation = @project.conversation
    @chat_messages = ChatMessage.where(conversation: @conversation).limit(5).reorder("#{ChatMessage.table_name}.created_at DESC").to_a
  end
  
  private
    def clear_old_records
      yesterday = Date.yesterday      
      old_chat_users = ChatUser.where("created_at <= ?", yesterday)
      old_chat_users.destroy_all 
    end

end
