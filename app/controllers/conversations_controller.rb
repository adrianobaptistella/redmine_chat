class ConversationsController < ApplicationController
  layout false
  before_action :find_project_by_project_id, :only => [:create]

  def create
    (render_403; return false) unless @project.module_enabled?(:redmine_chat) && User.current.allowed_to?(:launch_chat, @project)
    if Conversation.where(:project_id => @project.id).present?
      @conversation = Conversation.where(:project_id => @project.id).first
    else
      @conversation = Conversation.create!(conversation_params)
      @conversation.project_id = @project.id
    end
    manual_flag = params[:manual]
    if manual_flag == "1"
      render json: { conversation_id: @conversation.id, manual: manual_flag }
    else 
      render json: { conversation_id: @conversation.id }
    end
  end

  def show
    @conversation = Conversation.find(params[:id])
    @project = @conversation.project
    (render_403; return false) unless @project && @project.module_enabled?(:redmine_chat) && User.current.allowed_to?(:launch_chat, @project)
    if (params[:manual] == "1")
      @manual_flag = true
    else
      @manual_flag = false
    end
    # get the setting's max number
    @limit = 5
    if !ChatSetting[:max_number_in_chat, @project.id].blank?
      @limit = ChatSetting[:max_number_in_chat, @project].to_i
    end
    @messages = ChatMessage.where(conversation: @conversation).limit(@limit).reorder("#{ChatMessage.table_name}.created_at DESC").to_a
    @messages.reverse!
    @message = ChatMessage.new
  end

  private
    def conversation_params
      params.permit(:sender_id, :project_id)
    end
  
    def interlocutor(conversation)
      User.current == conversation.recipient ? conversation.sender : conversation.recipient
    end
end
