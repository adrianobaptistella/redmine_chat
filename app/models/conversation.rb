class Conversation < ActiveRecord::Base
  belongs_to :project
  belongs_to :sender, :foreign_key => :sender_id, class_name: 'User'
  belongs_to :recipient, :foreign_key => :recipient_id, class_name: 'User'

  has_many :chat_messages, dependent: :destroy

  validates_presence_of :project_id
  
end