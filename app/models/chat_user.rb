class ChatUser < ActiveRecord::Base
  belongs_to :project
  belongs_to :user
  belongs_to :conversation
end
