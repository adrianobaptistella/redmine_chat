Rails.configuration.to_prepare do
  # patches
  require_dependency 'chat/patches/user_patch'
  require_dependency 'chat/patches/project_patch'
  require_dependency 'chat/patches/projects_helper_patch'
  require_dependency 'chat/chat_project_setting'
  
  # hooks
  require_dependency 'chat/hooks/views_layouts_hook'
end

module RedmineChat

  def self.settings() Setting[:plugin_redmine_chat].blank? ? {} : Setting[:plugin_redmine_chat]  end


end
